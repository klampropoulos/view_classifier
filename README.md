
![[python-badge]](https://img.shields.io/badge/python-v.3.6-brightgreen.svg?fbclid=IwAR2SgN_cMg9AiARXhG-Fb9NBdHJsS-qVoYxtlraqrzsOf0xjiJ_q7ocQTPg)    ![[pytorch-badge]](https://img.shields.io/badge/pytorch-v.%201.1-brightgreen.svg)


*View_classifier*
=================



## *Links*:
* #### [Overview](#description)
* #### [Data location](#dataset-location)
* #### [Views](#angle-views)
* ####  [Dependencies](#requirements)



<br>

<br>



## *Description*:

### *ConvNet* view_classifier for *OuluVS2* dataset.


<br> 
<hr>

![](extras/mouth.gif)

<br>






## *Dataset Location:*

<br>

#### (need permission to use)

<br>

#### [OULUVS2:HOME](http://www.ee.oulu.fi/research/imag/OuluVS2/)

<br>

<hr>

## Angle views:

### *0°* angle,*30°* angle,    *45°* angle,    *60°* angle,   *90°* angle


![](extras/merged.png)



<hr>



## *Results*:



### Conf matrix of views when classified by first frame only!

![Alt text](extras/conf_mat.png)

## *Requirements*: 

#### **[requirements.txt](requirements.txt)**

<br>

#### **Install python requirements**:

```shell
pip install -r requirements.txt
```

<hr>


<br>

