import os
import matplotlib.pyplot as plt
import numpy as np
import shutil
from tqdm import tqdm

# Torch related libs
import torch
import torchvision
import torch.nn  as nn
from torch.autograd import Variable
from torchvision import transforms
from torch.utils.data import DataLoader, Dataset
from torchvision.transforms import Resize, Grayscale, ToTensor, Compose, CenterCrop, ToPILImage, Normalize
from torchvision.utils import save_image
import torch.backends.cudnn as cudnn
import torch.nn.functional as F 
from torch.utils.tensorboard import SummaryWriter

from data import Vd_Dataset


class VC(nn.Module):

    def __init__(self):
        super(VC, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 5, 1)
        self.conv2 = nn.Conv2d(32, 128, 5, 1)
        self.fc1 = nn.Linear(4*7*128, 250)
        #self.fc1 = nn.Linear(4*7*128, 500)
        self.fc2 = nn.Linear(250,5)
        #self.fc2 = nn.Linear(500,5)

    def forward(self, x):
        x1 = F.relu(self.conv1(x))
        x1_pool = F.max_pool2d(x1, 2, 2)
        x2 = F.relu(self.conv2(x1_pool)) 
        #x2 = F.dropout(x2, p=0.1)  # new layer
        x2_pool = F.max_pool2d(x2, 2, 2)
        x3 = x2_pool.view(-1, 4*7*128)
        x4 = F.relu(self.fc1(x3))
        x5 = self.fc2(x4)
        return F.log_softmax(x5, dim=1)


class VC2(nn.Module):

    def __init__(self):
        super(VC2, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 5, 1)
        self.conv1_bn = nn.BatchNorm2d(32)
        self.conv2 = nn.Conv2d(32, 128, 5, 1)
        self.conv2_bn = nn.BatchNorm2d(128)
        self.fc1 = nn.Linear(23*33*128, 5)
        self.fc1_bn = nn.BatchNorm1d(5)      

    def forward(self, x):
        x1 = F.relu(self.conv1_bn(self.conv1(x)))
        x2 = F.relu(self.conv2_bn(self.conv2(x1)))
        #x1 = F.relu(self.conv1(x))
        #x2 = F.relu(self.conv2(x1))
        x3 = x2.view(-1, 23*33*128)
        #x3 = F.dropout(x3, p=0.1)
        #x3 = self.fc1_bn(x3)
        x4 =  self.fc1(x3)
        #x4 = self.fc1_bn(x4)
        return F.log_softmax(x4, dim=1)


class VC3(nn.Module):

    def __init__(self):
        super(VC3, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 5, 1)
        self.conv1_bn = nn.BatchNorm2d(32)
        self.conv2 = nn.Conv2d(32, 64, 5, 1)
        self.conv2_bn = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(64, 128, 5, 1)
        self.conv3_bn = nn.BatchNorm2d(128)
        self.fc1 = nn.Linear(19*29*128, 5)
        self.fc1_bn = nn.BatchNorm1d(5)      


    def forward(self, x):
        x1 = F.relu(self.conv1_bn(self.conv1(x)))
        x2 = F.relu(self.conv2_bn(self.conv2(x1)))
        x3 = F.relu(self.conv3_bn(self.conv3(x2)))
        #x1 = F.relu(self.conv1(x))
        #x2 = F.relu(self.conv2(x1))
        #x3 = F.relu(self.conv3(x2))
        #print(x3.size())
        x3 = x3.view(-1, 19*29*128)
        #x3 = F.dropout(x3, p=0.1)
        #x3 = self.fc1_bn(x3)
        x4 =  self.fc1(x3)
        #x4 = self.fc1_bn(x4)
        return F.log_softmax(x4, dim=1)


###TODO: add argument parser

cudnn.benchmark = True 
####### Default config variables #############3
num_epochs = 20
BATCH_SIZE =1024 #512
learning_rate = 1e-3
##### Transforms ####
to_pil = ToPILImage()
normal = Normalize([0.5], [0.5])
scale = Resize((31, 41))
gray = Grayscale(num_output_channels=1)
to_tensor = ToTensor()
composed = Compose([scale, gray, to_tensor, normal])

##### Data initialization #######
root_dir = "train_folder"
train_dt = Vd_Dataset(root_dir, transform=composed)
#print(len(train_dt))
train_loader = DataLoader(train_dt, batch_size=BATCH_SIZE, num_workers=4, pin_memory=True, shuffle=True)

######## Validation #################
#test_dt = Vd_Dataset(root_dir, mode="dev", transform=composed)
#print(len(test_dt))
#test_loader = DataLoader(test_dt, batch_size=BATCH_SIZE, num_workers=1, pin_memory=True, shuffle=True)

### New data/loader  test 1st only  frame
test_frst_dt = Vd_Dataset("test_folder", transform=composed)
test_frst_loader = DataLoader(test_frst_dt, batch_size=BATCH_SIZE, num_workers=1, pin_memory=False)

#### add test_full_loader #####
test_full_dt = Vd_Dataset("test_full_folder", transform=composed)
test_full_loader = DataLoader(test_full_dt, batch_size=BATCH_SIZE, num_workers=2, pin_memory=True)

#### Choose model here
#vd = model.cuda()
# Model initialization

vd = VC3().cuda()

optimizer = torch.optim.Adam(
    vd.parameters(), lr=learning_rate, weight_decay=1e-5
)


def train(epoch, writer, output_dir):
    vd.train()
    train_loss = 0

    if epoch == 0:
        for batch_idx , (data,target) in enumerate(train_loader):

            data, target = data.cuda(), target.cuda()
            output = vd(data)
            loss = F.nll_loss(output, target)
            train_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch lo
            optimizer.zero_grad()
            print("hello")
        train_loss /= len(train_loader.dataset)
        writer.add_scalar('train_loss', train_loss, epoch)
        return train_loss
        
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.cuda(), target.cuda()
        optimizer.zero_grad()
        output = vd(data)
        loss = F.nll_loss(output, target)
        train_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
        loss.backward()
        optimizer.step()
        if batch_idx % 10 ==0: #args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
    train_loss /= len(train_loader.dataset)
    print(f"\nTrain set: Average loss @ epoch = {epoch} => {train_loss:.8f}")
    writer.add_scalar('train_loss', train_loss, epoch)
    # do checkpointing
    torch.save(vd.state_dict(),"%s/vd_epoch_%d.pth" %(output_dir, epoch))

    return train_loss


def valid(epoch, writer):
    """Validation data testing"""
    vd.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.cuda(), target.cuda()
            output = vd(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()
    print("valid")
    test_loss /= len(test_loader.dataset)
    test_acc = 100. * correct / len(test_loader.dataset)
    print('\nValid set: Average loss: {:.4f}, Accuracy: {}/{} ({:.1f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
    print(f"\nValid set: Average loss @ epoch = {epoch} => {test_loss:.8f}\n")
    writer.add_scalar('valid_loss', test_loss, epoch)
    writer.add_scalar('valid_acc', test_acc, epoch)


def test_frst(epoch, writer):
    """Test on extracted first frames data."""
    vd.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_frst_loader:
            data, target = data.cuda(), target.cuda()
            output = vd(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_frst_loader.dataset)
    test_acc = 100. * correct / len(test_frst_loader.dataset)
    print('\nTest_frst set: Average loss: {:.4f}, Accuracy: {}/{} ({:.1f}%)\n'.format(
        test_loss, correct, len(test_frst_loader.dataset),
        100. * correct / len(test_frst_loader.dataset)))
    print(f"\nTest_frst set: Average loss @ epoch = {epoch} => {test_loss:.8f}\n")
    writer.add_scalar('test_frst_loss', test_loss, epoch)
    writer.add_scalar('test_frst_acc', test_acc, epoch)


def test_full(epoch, writer):
    """Test on full test data."""
    vd.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_full_loader:
            data, target = data.cuda(), target.cuda()
            output = vd(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_full_loader.dataset)
    test_acc = 100. * correct / len(test_full_loader.dataset)
    print('\nTest_full set: Average loss: {:.4f}, Accuracy: {}/{} ({:.1f}%)\n'.format(
        test_loss, correct, len(test_full_loader.dataset),
        100. * correct / len(test_full_loader.dataset)))
    print(f"\nTest_full set: Average loss @ epoch = {epoch} => {test_loss:.8f}\n")
    writer.add_scalar('test_full_loss', test_loss, epoch)
    writer.add_scalar('test_full_acc', test_acc, epoch)




if __name__ == "__main__":

    # Writer will output to ./runs/ directory by default
    if os.path.isdir("./out"):
        shutil.rmtree("out")
    if os.path.isdir("./runs"):
        shutil.rmtree("runs")

    writer = SummaryWriter('runs')
    output_dir = "out"
    try:
        os.makedirs(output_dir, exist_ok=True)
    except OSError:
        pass

    resil = 5
    
    for epoch in range(num_epochs):
        loss = train(epoch, writer, output_dir)
        #valid(epoch, writer)
        test_frst(epoch, writer)
        #test_full(epoch, writer)
        if epoch == 0:
            last_loss = loss
        if last_loss < loss:
            resil= resil - 1
        if resil == 0:
            print("resil = 0..Breaking...")
            break
        last_loss = loss

    writer.close()
    

