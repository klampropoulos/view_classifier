import os
from os.path import join

from PIL import Image 
import torch
from torch.utils.data  import Dataset, DataLoader
import torchvision.transforms as transforms
import numpy as np
import matplotlib.pyplot as plt
from torchvision.transforms import Resize, Grayscale, ToTensor, Compose, CenterCrop, ToPILImage

# Custom folders
from utils import file_is_image, find_angle



class Vd_Dataset(Dataset):


    def __init__(self, image_dir, split_factor=1.0, mode="train", transform=None):
        """
        Args:

            image_dir : folder og images (str)
            
            split_factor : (float)

            mode : train/test (str)

            transform: list of (torchvision.transforms)
            
        """
        super(Vd_Dataset, self).__init__()
        image_filenames = [x for x in os.listdir(image_dir) if  file_is_image(x)]
        
        image_angles = [find_angle(image) - 1 for image in image_filenames]

        self.num_labels = len(set(image_angles)) #new code
        
        image_filenames =[ os.path.join(image_dir, img) for img in image_filenames ]
        
        if image_dir != "test_folder":
            total_number = len(image_filenames)
            splt_ind = int(split_factor * total_number)
            if mode == "train":    
                self.image_filenames = image_filenames[:splt_ind]
                self.image_angles = image_angles[:splt_ind]

            else:
                self.image_filenames = image_filenames[splt_ind:]
                self.image_angles = image_angles[splt_ind:]

        else:
            #print("Test mode!!!")
            self.image_filenames = image_filenames
            self.image_angles = image_angles

        if transform is not None:
            self.transform = transform


    def __getitem__(self, index):
        """
        Args:
            index : int

        Returns:
            tuple
        """
        img_label = self.image_filenames[index]
        img = Image.open(img_label)
        if self.transform:
            img = self.transform(img)
        img_angle = self.image_angles[index]
        img_angle = img_angle
        return img, img_angle

    
    def __len__(self):
        return len(self.image_filenames)



if __name__ == "__main__":
    root_dir = "train_folder"
    scale = Resize((31, 41))
    gray = Grayscale(num_output_channels=1)
    #to_pil = ToPILImage()
    to_tensor = ToTensor()
    composed = Compose([scale, gray, to_tensor])
    BATCH_SIZE = 64
    train_dt = Vd_Dataset(root_dir, transform=composed)
    train_loader = DataLoader(train_dt, batch_size=BATCH_SIZE, num_workers=4, shuffle=True)

    
    for batch_idx, (data, target) in enumerate(train_loader):
        print(type(target))
        print(target.size())
    """    
    for i in range(len(train_dt)):
        print(type(train_dt[i]))
        img , y = train_dt[i]
        print(type(y), y) # int 
    """