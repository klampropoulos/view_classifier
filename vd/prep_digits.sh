#! /bin/bash


ffmpeg_inst=`which ffmpeg | grep "not found"`
if [ ! -z "$ffmpef_inst" ] ; then
	# Assuming Debian distro
	echo "Trying to install ffmpeg package!!"
	sudo apt install ffmpeg -y || exit 1;
	# Assuming Centos
	#sudo yum install ffmpeg ffmpeg-devel -y
	# Assuming Fedora
	#sudo dnf install ffmpeg ffmpeg-devel -y 

fi



 

if [ ! -d "digits"  ] ; then
	echo "Linking cropped_mouth_mp4_digit here"
	read -p "Please give me the relative path to the video-digits folder: " rel_path
	: "${rel_path:=../../OuluVS2/cropped_mouth_mp4_digit}"
	ln -s $rel_path digits ||  exit 1;
	
fi


read -p "Please give me mode(train/test/test_full): " mode
: "${mode:=train}"

echo Mode is "$mode"
sleep 5

n_speak=41
if [ "$mode" = train ] 
then 
	echo "Train mode!!!"
	#if [ ! -d "train_folder" ] ; then

		##mkdir train_folder || exit 1;

		for j in {1..5} ; do
			
			for i in {1..41} ; do
				
				path=digits/$i/$j
				curr_files=`ls $path/ | cut -d"." -f1`
				echo $curr_files
				#for file in $curr_files; do
				#	echo $path/$file.mp4
				#done
				#exit 1;
				for file in $curr_files; do
					#ffmpeg -i $path/$file.mp4 -vf fps=fps=30 -vf hue=s=0 train_folder/$i/$file%03d.png || exit 1;   #stopped here
					ffmpeg -i $path/$file.mp4 -vf fps=fps=30  train_folder/$file%03d.png || exit 1;   #stopped here
				done
			done

			


		done

	#fi
fi
: '
elif [ "$mode" = test ]
then 
	echo "Test mode!!"
	sleep 5
	if [ ! -d "test_folder" ] ; then

		mkdir test_folder || exit 1;
   
		for j in {1..5} ; do
			
			for i in {42..53} ; do
				
				path=video/$i/$j
				curr_files=`ls $path/ | cut -d"." -f1`
				echo $curr_files
				#for file in $curr_files; do
				#	echo $path/$file.mp4
				#done
				#exit 1;
				for file in $curr_files; do
					ffmpeg -i $path/$file.mp4 -vframes 1  -vf fps=fps=30  test_folder/$file%03d.png || exit 1;   #stopped here
				done
			done

		done

	fi

elif [ "$mode" = test_full ]
then
	echo "Test full mode!!"
	sleep 5
	if [ ! -d "test_full_folder" ] ; then

		mkdir test_full_folder || exit 1;
   
		for j in {1..5} ; do
			
			for i in {42..53} ; do
				
				path=video/$i/$j
				curr_files=`ls $path/ | cut -d"." -f1`
				echo $curr_files
				#for file in $curr_files; do
				#	echo $path/$file.mp4
				#done
				#exit 1;
				for file in $curr_files; do
					ffmpeg -i $path/$file.mp4   -vf fps=fps=30  test_full_folder/$file%03d.png || exit 1;   #stopped here
				done
			done

		done

	fi


else
	echo "Mode error!!!!!!"
	exit 1;
fi

'



echo ===============
echo  " $0 ended at  `date` ";
exit 0;
