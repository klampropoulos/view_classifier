import os
import matplotlib.pyplot as plt
import numpy as np
import shutil
import argparse
import sys
import seaborn as sns
import pandas as pd
from torchviz import make_dot
# Torch related libs
import torch
import torchvision
import torch.nn  as nn
from torch.autograd import Variable
from torchvision import transforms
from torch.utils.data import DataLoader, Dataset
from torchvision.transforms import Resize, Grayscale, ToTensor, Compose, CenterCrop, ToPILImage, Normalize
from torchvision.utils import save_image
import torch.backends.cudnn as cudnn
import torch.nn.functional as F 
from torch.utils.tensorboard import SummaryWriter

# Custom imports
from data import Vd_Dataset
from classifier import VC, VC2, VC3


def find_best_model(dir):
    """
    Args:
        dir:str
    Returns:
        fullpath to file: str
    """

    filenames = os.listdir(dir)
    #print(filenames)
    file_int = [int(filename.split(".")[0].split("_")[-1]) for filename in filenames]
    #print(file_int)
    max_filename = filenames[file_int.index(max(file_int))]
    #print(max_filename)
    #print(os.path.join(dir,max_filename))
    return os.path.join(dir, max_filename)


def make_conf_matrix(n_classes, model, data_loader):
    """
    Args:
        n_classes : int

        model : torch model

        data_loader : torch dataloader
    Returns:
        conf_np :  numpy array => n_classes X n_classes
    """
    conf_mat = torch.zeros(n_classes, n_classes)
    with torch.no_grad():
        for inputs, classes in data_loader:
            inputs=inputs.cuda()
            classes = classes.cuda()
            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)
            for t, p in zip(classes.view(-1), preds.view(-1)):
                conf_mat[t.long(), p.long()] +=1
    
    conf_np = conf_mat.numpy() 
    df_cm = pd.DataFrame(conf_np, index = [i for i in ["0°","30°","45°", "60°", "90°"]],
                            columns = [i for i in ["0°","30°","45°", "60°", "90°"]])
    plt.title("Confusion matrix of views")
    plt.xlabel("True")
    plt.ylabel("Predicted")
    sns.set(font_scale=1.4)
    model_name = model.__class__.__name__
    sns.heatmap(df_cm, annot=True, annot_kws={"size": 10}, fmt="g")
    fig_name = "../extras/"+model_name+"_conf_mat.png"
    #plt.savefig("../extras/conf_mat.png")
    plt.savefig(fig_name)
    return conf_np



def main():
    # Argument Parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("--images_dir", type=str, required=True, help="Path to images")
    parser.add_argument("--models_dir", type=str, required=True, help="Path to folder of checkpoint models!")
    parser.add_argument("--batch_size", type=int, default=256 , help="Batch size")
    parser.add_argument("--spec_model", type=str, help="specific model")
    opt = parser.parse_args()
    print(opt)
    # Find best chekpoint model
    #print(opt.models_dir)
    #print(os.listdir(opt.models_dir))
    #sys.exit()
    if opt.spec_model is not None:
        model_path = os.path.join(opt.models_dir, opt.spec_model)
    else:
        model_path = find_best_model(opt.models_dir)
    # Load model
    #vd = VC().cuda()
    vd = VC2().cuda()
    vd.load_state_dict(torch.load(model_path))
    # Print model parameters
    for module in vd.modules():
        print(module)
    for child in vd.children():
        print(child)
    #set eval mode
    vd.eval()
    # prepare  test data
    normal = Normalize([0.5], [0.5])
    scale = Resize((31, 41))
    gray = Grayscale(num_output_channels=1)
    to_tensor = ToTensor()
    compose = Compose([scale, gray, to_tensor, normal])
    test_dt = Vd_Dataset(opt.images_dir, transform=compose)
    ########################
    n_classes = test_dt.num_labels
    #print("Number of classes: ",str(n_classes))

    # First frame only test data
    test_loader = DataLoader(test_dt, batch_size=opt.batch_size, num_workers=4,pin_memory=True)
    conf_mat = make_conf_matrix(n_classes, vd, test_loader)

    # make graph 
    batch_in , batch_class = next(iter(test_loader))
    #hl_graph = hl.build_graph(vd, batch_in.cuda())
    out = vd(batch_in.cuda())
    g = make_dot(out)
    g.view()
    


if __name__ == "__main__":
    main()