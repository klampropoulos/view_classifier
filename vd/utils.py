import os
import functools
import numpy as np 
from os.path import join
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import argparse



def file_is_image(filename):
    """Check if file is image.
    Args:
        filename: str
    Returns:
        bool
    """
    return any(filename.endswith(extension) for extension in [".png", ".jpg", ".jpeg"])


def find_angle(filename):
    """Find type of view from file.
    Args:
        filename: str
    Returns:
        angle: int
    """
    return int(list(filename.split("_")[1])[1]) # "s2_v2_u32005.png" ==> v2 ==> 2


def list_files(root_dir):
    """
    Args:
        root_dir: (str) relative path to folder
    Returns:
         list of str
    """
    return os.listdir(root_dir)


def diff_image(image, ax):
    """
        image: numpy.ndarray

        ax: int
    """
    diff_image = np.diff(image,axis=ax)

    return diff_image



def plot_acc(csv_file):
    """
    Args:
        csv_file: str
    """
    #plt.style.use('ggplot')
    sns.set_style('whitegrid')
    df = pd.read_csv(csv_file)
    acc = df["Value"].values
    length = len(acc)
    plt.title("Accuracy vs. epoch")
    plt.ylabel("Test acc(%)")
    plt.xlabel("Epoch")
    plt.xticks(range(0,length))
    plt.plot(range(0,length), acc)
    #plt.plot(range(1,16), acc) htan etsi
    png_file = csv_file.split(".")[0] + ".png"
    save_path = "../extras/" + png_file
    plt.savefig(save_path)
    #plt.savefig("../extras/test_acc.png")
    plt.show()


def plot_loss(csv_file):
    """
    Args:
        csv_file: str
    """
    #sns.set_style('ggplot')
    plt.style.use('ggplot')
    df = pd.read_csv(csv_file)
    acc = df["Value"].values
    length = len(acc)
    plt.title("Negative-log likelihood")
    plt.ylabel("Test loss")
    plt.xlabel("Epoch")
    plt.xticks(range(0,length))
    plt.plot(range(0,length), acc)
    png_file = csv_file.split(".")[0] + ".png"
    save_path = "../extras/" + png_file
    plt.savefig(save_path)
    plt.show()


def plot_img():
    pass


if __name__ == "__main__":
    """
    ex_file = "s2_v2_u32005.png"
    ex2_file = "s2_v4_u32005.png"
    res = file_is_image(ex_file) # bool
    print(f"res={res} of type={type(res)}!!")
    tokens = ex_file.split("_")
    angle_str = int(list(tokens[1])[1])
    print(angle_str,type(angle_str))
    print(tokens,type(tokens))
    angle = find_angle(ex_file)
    print(angle, type(angle))
    angle2 = find_angle(ex2_file)
    print(angle2, type(angle2))
    """
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--acc", type=str, default="test_acc.csv", help="plot accuracy")
    parser.add_argument("--loss", type=str, default="train_loss.csv", help="plot_loss")
    opt = parser.parse_args()
    print(opt)

    #csv_filename = "test_acc.csv"
    #plot_acc(csv_filename)
    plot_acc(opt.acc)
    #csv_filename = "train_loss.csv"
    #plot_loss(csv_filename)
    plot_loss(opt.loss)

